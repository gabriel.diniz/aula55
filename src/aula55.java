public class aula55 { // arrays multidimensionais III

    public static void main(String[] args) {
        int [][] nums = {{1,2,3},  // array  nums 2 dimensões, array 3 por 3
                        {4,5,6},
                        {7,8,9}};
       // int [][] x = new int[2][2];
        // laço de repetição
    for(int x = 0; x<3; x++){  // looping for, variavel x, x menor que 3, cada ciclo 1 unidade array
        for(int y = 0; y<3; y++) // for, variavel y, x menor que 3, cada ciclo 1 unidade array
        System.out.print(nums[x][y]+" "); // imprime cada valor com espaço
        System.out.println(); // quebra linha a cada execução do laço
        }
    }
}
